#include <Arduino.h>
#include <SoftwareSerial.h>

#include "SerialMaster.h"
#include "SerialSlave.h"



// Tested Options for STM32
// RX: PB8, PC15
// TX: PB7, PC14

//#define MASTER

#ifdef MASTER
#define TX  PC15
#define RX  PC14
#else
#define SLAVE
#define TX  PB8
#define RX  PB7
#endif


SoftwareSerial mySerial = SoftwareSerial(RX, TX);


bool callback(const SERCOMCommand &request, SERCOMCommand &response);

namespace ph = std::placeholders;
auto boundCallback = std::bind(callback, ph::_1, ph::_2);


#ifdef MASTER
SerialMaster sercom = SerialMaster(mySerial);
#else
SerialSlave sercom = SerialSlave(mySerial);
#endif

SERCOMCommand cmd;

void setup() {
  
  mySerial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  mySerial.println("Start");
}

void loop() {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  delay(200);
  
  

#ifdef MASTER
  cmd.clear();
  cmd.push_back("cmd");
  cmd.push_back("1");
  cmd.push_back("2");

  
  if (sercom.Send(cmd))
  {
  
    // receive response
    if (sercom.Receive(cmd))
    {
        for (auto &c : cmd)
        {
          mySerial.println(c);
          mySerial.println("============");
        }
        
    }
    else
    {
      mySerial.println("Polling error");
      mySerial.println("============");
    }

  }
  else
  {
    mySerial.println("Not ACKed");
    mySerial.println("============");
  }

#else
  sercom.Listen(boundCallback);
#endif

}

bool callback(const SERCOMCommand &request, SERCOMCommand &response)
{
  mySerial.println("Received:");
  for (auto &c : request)
  {
    mySerial.println(c);
  }

  mySerial.println();
  mySerial.println("============");

  response.push_back("OK");
  for (auto &c : request)
  {
    response.push_back(c);
  }

  

  return true;
}
