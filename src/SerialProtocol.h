#pragma once

#include <Arduino.h>

/**
 * @brief Define special, non-printable characters to be used as control signals
 * 
 */

// activate for µC to µC. The alternative below is better for human testing
#define SERCOM_MSG_START   0x2   // marks start of message
#define SERCOM_MSG_END     0x3   // marks end of message
#define SERCOM_MSG_TERM    0x4   // marks end of transmission
#define SERCOM_MSG_DELIM   0x37  // list delimiter (e.g., for parameters)
#define SERCOM_MSG_ACK     0x6   // acknowledgement of message


// for testing
/*
#define SERCOM_MSG_START   '<'   // marks start of message
#define SERCOM_MSG_END     '>'   // marks end of message
#define SERCOM_MSG_TERM    '#'   // marks end of transmission
#define SERCOM_MSG_DELIM   '|'  // list delimiter (e.g., for parameters)
#define SERCOM_MSG_ACK     '@'   // acknowledgement of message
*/


// timeout in millis, configurable via cflags
#ifndef SERCOM_TIMEOUT
#define SERCOM_TIMEOUT     100     
#endif

// 100 characters maxium length for memory management, configurable via cflag -DSERCOM_MSG_MAX_LEN
#ifndef SERCOM_MSG_MAX_LEN
#define SERCOM_MSG_MAX_LEN 100     
#endif
