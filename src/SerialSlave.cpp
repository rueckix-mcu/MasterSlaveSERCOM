#include "SerialSlave.h"

SerialSlave::SerialSlave(SoftwareSerial &serial)
:_serial(serial)
{
    
}

SerialSlave::~SerialSlave()
{
 
}

bool SerialSlave::Receive(SERCOMCommand& cmd)
{
    _serial.listen();
    

    if (_serial.available() > 0)
    {
        String msg = _serial.readStringUntil(SERCOM_MSG_TERM);
        if (SerialMessage::Decode(msg, cmd))
        {
            // send ack (message will be dispatched)
            _serial.write(SERCOM_MSG_ACK);
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }

}

void SerialSlave::Listen(SERCOMCommandHandler handler)
{    
    // enter blocking loop
    while (true)
    {
        SERCOMCommand request;

        // wait for data to become available
        while(_serial.available() == 0 ){}
        
        if (Receive(request))
        {
            SERCOMCommand response;
            if (handler(request, response))
            {
                String msg;
                SerialMessage::Encode(response, msg);
                _serial.print(msg);
                _serial.write(SERCOM_MSG_TERM);
            }
        }
    }
}


void SerialSlave::Respond(SERCOMCommand& response)
{
    String msg;
    SerialMessage::Encode(response, msg);
    _serial.print(msg);
    _serial.write(SERCOM_MSG_TERM);

    // slaves don't wait for ack
}