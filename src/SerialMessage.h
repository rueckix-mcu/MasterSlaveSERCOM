#pragma once

#include "SerialProtocol.h"
#include <vector>

typedef String SERCOMMessage;
typedef std::vector<String> SERCOMCommand;

class SerialMessage
{
public:
    /**
     * @brief Encodes a command into a protocol string
     * 
     * @param cmd input command
     * @param msg output string
     */
    static void Encode(const SERCOMCommand &cmd, SERCOMMessage &msg);

    /**
     * @brief Decodes a string into a protocol command
     * 
     * @param msg input string
     * @param cmd output command
     * 
     * @return true if there was a message available to decode
     */
    static bool Decode(const SERCOMMessage &msg, SERCOMCommand &cmd);


};

