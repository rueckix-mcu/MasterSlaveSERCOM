#include "SerialMessage.h"

void SerialMessage::Encode(const SERCOMCommand &cmd, SERCOMMessage &msg) {
    
    msg = "";
    msg.concat(SERCOM_MSG_START);

    for (auto &i : cmd)
    {
        msg.concat(i);
        msg.concat(SERCOM_MSG_DELIM);
    }

    msg.concat(SERCOM_MSG_END);    
}

bool SerialMessage::Decode(const SERCOMMessage &msg, SERCOMCommand &cmd) {
    cmd.clear();

    
    // look for start marker
    int start_pos = msg.indexOf(SERCOM_MSG_START);
    if (start_pos == -1)
        return false;

    // look for end marker
    int end_pos = msg.indexOf(SERCOM_MSG_END);
    if (end_pos == -1)
        return false;

    // no content
    if (end_pos == start_pos + 1)
        return false;

    int next = 0;

    // parse message of size >= 1
    bool hasNext = true;
    do
    {
        
        next = msg.indexOf(SERCOM_MSG_DELIM, start_pos+1);

        // no more delimiters
        if (next == -1 || next >= end_pos)
        {
            hasNext = false;
            next = end_pos;
        }
        
        if (start_pos + 1 < next)
        { // extract non-empty substring, ending index is EXCLUSIVE
            cmd.push_back(msg.substring(start_pos + 1, next));
        }

        start_pos = next;
                   
    } while (hasNext);

    return true;
}
