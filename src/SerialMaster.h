#pragma once

#include "SerialMessage.h"
#include <SoftwareSerial.h>


class SerialMaster
{
private:
    SoftwareSerial &_serial;

public:
    SerialMaster(SoftwareSerial &serial);
    ~SerialMaster();

public:
    /**
     * @brief Send command over the Stream
     * 
     * @param cmd 
     * @return true if the command was acknowledged, false upon timeout or if there was no command to send
     */
    bool Send(const SERCOMCommand& cmd);

    /**
     * @brief Receive the slave for a command. 
     * @return true if a command was received, false if the command was empty (nothing more to receive)
     */
    bool Receive(SERCOMCommand& cmd);
    
};

