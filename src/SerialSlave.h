#pragma once

#include "SerialMessage.h"
#include <SoftwareSerial.h>



/**
 * @brief Function template for callbacks from the Listen() method
 * The callback is expected to handle the request and return a response.
 * If the callback encouters an error, it shall return false.
 * 
 */
// define FUNCTIONAL for using std::function instead
#ifdef FUNCTIONAL 
#include <functional>
typedef std::function<bool(const SERCOMCommand &request, SERCOMCommand &response)> SERCOMCommandHandler;
#else
typedef bool (*SERCOMCommandHandler)(const SERCOMCommand &request, SERCOMCommand &response);
#endif


class SerialSlave
{
private:
    SoftwareSerial &_serial;

public:
    SerialSlave(SoftwareSerial &serial);
    ~SerialSlave();

public:
    /**
     * @brief Listen for command from the master node. Blocks until a message has been received
     * 
     * @param handler returns the received command
     * 
     */
    void Listen(SERCOMCommandHandler handler);

    /**
     * @brief Try to receive a command from the master node. Non blocking.
     * 
     * @param cmd       - command received
     * @return true     - iff a command was received.
     * @return false 
     */
    bool Receive(SERCOMCommand& cmd);

    /**
     * @brief Send a response to the master node. ATTN: Note that the Master is not waiting for messages by default. Users of this function must ensure this via a higher-level protocol convention.
     * 
     * @param response - command to send
     */
    void Respond(SERCOMCommand& response);

    
};

