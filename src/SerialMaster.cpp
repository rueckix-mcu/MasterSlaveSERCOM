#include "SerialMaster.h"

SerialMaster::SerialMaster(SoftwareSerial &serial)
:_serial(serial)
{
    
}

SerialMaster::~SerialMaster()
{
    
}

bool SerialMaster::Send(const SERCOMCommand &cmd)
{
    SERCOMMessage msg;
    // check if we received an ACK on time
    bool ack = false;

    if (cmd.size() == 0)
        return false; // nothing to send

    SerialMessage::Encode(cmd, msg);

    _serial.print(msg);
    _serial.write(SERCOM_MSG_TERM);
    


    
    uint32_t now = millis();
    while (millis() - now < SERCOM_TIMEOUT)
    {
        if (_serial.available() > 0 && _serial.read() == SERCOM_MSG_ACK)
        {
            ack = true;
            break;
        }
    }

    return ack;    
}

bool SerialMaster::Receive(SERCOMCommand& response)
{
    
    uint32_t now = millis();
    bool ok = false;

    while (millis() - now < SERCOM_TIMEOUT)
    {
        if (_serial.available() > 0)
        {
            ok = true;
            break;
        }
    }

    if (!ok)
        return false; // timeout


    // now read and return the next command.
    SERCOMMessage msg = _serial.readStringUntil(SERCOM_MSG_TERM);
    
    ok = SerialMessage::Decode(msg, response);

    return ok;
}